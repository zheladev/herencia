/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pasodeparametros;

/**
 *
 * @author Zhel-PC
 */
public class main {
    public static void main(String[] args) {
        int pasadoPorValor = 10;
        int[] pasadoPorReferencia = {10};
        
        System.out.println("Paso por valor");
        
        System.out.println("\t valor antes de llamar a sumarInt: " + pasadoPorValor);
        sumarInt(pasadoPorValor);
        System.out.println("\t valor tras llamar a sumarInt: " + pasadoPorValor);
        
        System.out.println("\t valor antes de llamar a sumarIntRef: " + pasadoPorReferencia[0]);
        sumarIntRef(pasadoPorReferencia);
        System.out.println("\t valor tras llamar a sumarInt: " + pasadoPorReferencia[0]);
        
    }
    
    public static void sumarInt(int i) {
        i += 10;
    }
    
    public static void sumarIntRef(int[] i) {
        i[0] += 10;
    }
}
