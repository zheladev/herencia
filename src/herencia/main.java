/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Zhel-PC
 */
public class main {
    
    public static void main(String[] args) {
        BloqueAbstracto[] documento = new BloqueAbstracto[3];
    
        BloqueTexto bText = new BloqueTexto("Esto es un bloque de texto");
        BloqueTextoImagen bImg = new BloqueTextoImagen("dGRzIHB0cw==", 
                                                       "Este bloque lleva imagen");

        String[][] tabla = {{"A"}, {"B"}};
        BloqueTabla bTable = new BloqueTabla(tabla);
        
        documento[0] = bText;
        documento[1] = bImg;
        documento[2] = bTable;
        
        System.out.println("Documento sin indices:");
        //sin indices (override metodo abstracto)
        for (BloqueAbstracto bloque: documento) {
            System.out.println(bloque.getContent());
        }
        
        System.out.println("Documento con indices:");
        //con indices (override metodo definido en clase abstracta)
        for (BloqueAbstracto bloque: documento) {
            System.out.println(bloque.getContentWithIndex());
        }
        
        System.out.println("Llamando a metodo estatico getNumberTen()...");
        System.out.println(BloqueAbstracto.getNumberTen());
    }
}
