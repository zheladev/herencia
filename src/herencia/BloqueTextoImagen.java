/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Zhel-PC
 */
public class BloqueTextoImagen extends BloqueTexto{
    String b64_image; //esto no se guardaría como un string, es sólamente un ejemplo para no usar genéricos
    
    BloqueTextoImagen(String b64_image, String texto) {
        super(texto); //llamamos al constructor del padre BloqueTexto y le pasamos el parámetro texto
        this.b64_image = b64_image;
    }

    @Override //sobreescribimos el método anterior
    String getContent() {
        String contenido;
        contenido = "Texto: " + this.texto + "\n"; //usamos this.texto para acceder al valor texto del padre, ya que no es una variable privada
        contenido += "Imagen en base64: " + b64_image + "\n";
        return contenido;
    }
}
