/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Zhel-PC
 */
public abstract class BloqueAbstracto {
    static int bloquesCreados = 0;
    protected int index;
    private int privateIndex;
    
    BloqueAbstracto() {
        index = bloquesCreados++;
        privateIndex = index;
    }
    
    abstract String getContent();
    
    public String getContentWithIndex() {
        return "Indice: " + index + "\n" + this.getContent();
    }
    
    public static int getNumberTen() {
        return 10;
    }
}
