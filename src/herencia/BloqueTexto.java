/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Zhel-PC
 */
public class BloqueTexto extends BloqueAbstracto {
    String texto;
    
    BloqueTexto(String texto) {
        super(); //llamamos al constructor del padre BloqueAbstracto
        this.texto = texto; //guardamos el valor del parámetro texto en la variable texto de la clase BloqueTexto
    }

    @Override //sobreescribimos el método anterior
    String getContent() {
        return texto + "\n";
    }
    
    //no hace falta ya que se ha declarado en el padre (super)
//    @Override
//    public String getContentWithIndex() {
//        return super.getContentWithIndex() + this.getContent();
//    }
}
